(define-module (ise packages golang)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system go)
  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages base)
  #:export (go-github-com-gohugoio-hugo))

(define go-github-com-gohugoio-hugo
  (package
   (name "go-github-com-gohugoio-hugo")
   (version "0.105.0")
   (source (origin
             (method url-fetch)
             (uri "https://usc1.contabostorage.com/5e37e084f153485691f07db407003650:cx-src/hugo-0.105.tar.gz")
             (sha256
              (base32
               "13ls3j317nb54lfp6lm2iv2q0gzmdl20qzkav08xlk49951z948j"))))
   (build-system go-build-system)
   (arguments 
    `(#:go ,go-1.19
      #:import-path "github.com/gohugoio/hugo"))
   (home-page "https://github.com/gohugoio/hugo")
   (synopsis "Some Software written with golang")
   (description
    "a simple description")
   (license bsd-3)))
