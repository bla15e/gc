(define-module (cx build-system avax)
  #:use-module (guix gexp)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix packages)
  #:use-module (gnu packages guile)

  #:use-module (guix build-system go)
  #:use-module (guix build-system)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system copy)
  #:export (avax-node-build-system))


;; Module for budiling Avalanchego Node Guix Packages
;; avax-node-build-system

(define* (lower name
		#:key target system avalanchego plugins
		#:allow-other-keys
		#:rest arguments)
  (define private-keywords
    '(#:target #:inputs #:native-inputs))
  (bag
    (name name)
    (system system)
    (build copy-build)
    (arguments `(#:avalanchego ,avalanchego
                 #:plugins ,plugins))))

(define (default-avalanchego)
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((avax (resolve-interface '(ise packages avax))))
    (module-ref avax 'go-github-com-ava-labs-avalanchego)))

(define (default-avalanchego-plugins)
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((avax (resolve-interface '(ise packages avax)))
        (coreth (module-ref avax 'go-github-com-ava-labs-coreth)))
    `(("evm" . (file-append coreth "/bin/evm")))))


;; avax-gexp
(define avax-builder
  #~(begin
      ))

(define* (build-avax-node name inputs
                          #:key
                          (avalanchego (default-avalanchego))
			  (system (%current-system))
                          (plugins (default-avalanchego-plugins)))
  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name avax-builder
                      #:system system
                      #:target #f
                      #:graft? #f)))

(define avax-node-build-system
  (build-system
    (name 'avax-node)
    (description
     "Avalanchego build system, for running avalanchego nodes with arbitrary plugins")
    (lower lower)))

