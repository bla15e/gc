(define-module (ise home configurations bla15e)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services xdg)
  #:use-module (gnu packages)
  #:autoload  (gnu packages base) (glibc-utf8-locales)
  #:use-module (gnu packages chromium)
  #:use-module (gnu build chromium-extension)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages shellutils)
  #:use-module (gnu packages wm)
  #:use-module (gnu services)
  #:use-module (ise home services package-management)
  #:use-module (guix gexp)
  #:export (bla15e-home-env
            bla15e-zsh-service))



(define font-packages
  (map specification->package
       (list
	    "font-iosevka"
        "font-awesome")))

(define login-var-service
  (simple-service 'login-variables
                   home-environment-variables-service-type
                   `(("XDG_DATA_DIRS" . "$XDG_DATA_DIRS:/usr/local/share/:/usr/share/")
                     ;; ("XDG_CONFIG_DIRS" . "$XDG_CONFIG_DIRS:/etc/xdg/")
                     ;; ("XDG_CONFIG_DIRS" . "$HOME/.guix-home/profile/etc/xdg:$XDG_CONFIG_DIRS")
                     ("PATH" . "$HOME/.local/bin:$PATH"))))
;; Perhaps someday I'll have a
(define %bla15e-packages
  (list
   glibc-utf8-locales
   gnome-keyring
   guix

   gnutls nss-certs
   zsh-syntax-highlighting
   zsh-autosuggestions

   ;ungoogled-chromium-wayland
   ;ublock-origin-chromium
   ))

;; A riced out zsh configuration hell yeah
(define zsh-configuration
  (home-zsh-configuration
   (environment-variables
    '(("EDITOR" . "\"emacsclient -a -t''\"")
      ;("XCURSOR_THEME" . "Nordzy-cursors")
      ("GUIX_LOCPATH" . "$HOME/.guix-profile/lib/locale")
      ("GUIX_EXTRA_PROFILES" . "$HOME/.guix-extra-profiles")
      ("SSH_AUTH_SOCK" . "$XDG_RUNTIME_DIR/ssh-agent.socket")
      ("SSL_CERT_DIR" . "$HOME/.guix-home/profile/etc/ssl/certs")
      ("SSL_CERT_FILE" . "$HOME/.guix-home/profile/etc/ssl/certs/ca-certificates.crt")
      ("GIT_SSL_CAINFO" . "$SSL_CERT_FILE")
      ("GEM_PATH" . "$HOME/.local/share/gem")))
      ;; Not Necessary
      ;("_JAVA_AWT_WM_NONREPARENTING" . "1")
      ;("DIRSTACKSIZE" . "8")))
   (zshrc
    (list
     (local-file "../files/zshrc")))))

(define xdg-services
  (list
   (service home-xdg-mime-applications-service-type
            (home-xdg-mime-applications-configuration
             (added '(("application/pdf" . "chromium.desktop")
                      ("application/pdf" . "chromium.desktop")))
             (default '(("x-scheme-handler/http" . "chromium.desktop")
                        ("x-scheme-handler/https" . "chromium.desktop")
                        ("application/pdf" . "chromium.desktop")
                        ("text/plain" . "emacsclient.desktop")))
             (removed '())
             (desktop-entries '())))
   (service home-xdg-user-directories-service-type
            (home-xdg-user-directories-configuration
             (desktop     "$HOME/desktop")
             (documents   "$HOME/documents")
             (download    "$HOME/downloads")
             (music       "$HOME/music")
             (pictures    "$HOME/pictures")
             (publicshare "$HOME/public")
             (templates   "$HOME/templates")
             (videos      "$HOME/videos")))))


(define bla15e-zsh-service
  (service home-zsh-service-type
           zsh-configuration))


;; My old bash configuration
;; Need to change things
;        (service home-bash-service-type
;                 (home-bash-configuration
;                  (aliases
;                   '(("grep" . "grep --color=auto")
;                     ("ll" . "ls -l")
;                     ("ls" . "ls -p --color=auto")))
 ;                 (bashrc
 ;                  (list (local-file "config/.bashrc" "bashrc")))
  ;                (bash-profile
 ;                  (list (local-file
 ;                         "config/.bash_profile"
 ;                         "bash_profile")))))))))

(define %bla15e-home-services
  (cons*
   (service home-gwl-service-type)
    bla15e-zsh-service
    login-var-service
    xdg-services))

(define bla15e-home-env
  (home-environment
   (packages %bla15e-packages)
   (services %bla15e-home-services)))
