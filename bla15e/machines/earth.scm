(define-module (bla15e machines earth)
  #:use-module (gnu)
 
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)

  #:use-module (ise system services docker-compose)

  #:use-module (bla15e systems)
  #:export (earth-system
	    earth-machine))

(define earth-system
  (operating-system
    (inherit bla15e-system)
    (host-name "earth")
    (swap-devices (list (swap-space
			 (target (uuid
                                  "aad0fc05-287c-416c-b379-b251068e5994")))))

    ;; The list of file systems that get "mounted".  The unique
    ;; file system identifiers there ("UUIDs") can be obtained
    ;; by running 'blkid' in a terminal.
    (file-systems (cons* (file-system
                           (mount-point "/boot/efi")
                           (device (uuid "9876-55B7"
					 'fat32))
                           (type "vfat"))
			 (file-system
                           (mount-point "/")
                           (device (uuid
                                    "cd8075c1-2bf6-4a06-8205-a5a82a8258da"
                                    'ext4))
                           (type "ext4")) %base-file-systems))))

(define earth-machine
  (machine
   (operating-system earth-system)
   (environment managed-host-environment-type)
   (configuration (machine-ssh-configuration
                   (host-name "192.168.1.223")	  
                   (system "x86_64-linux")
                   (user "root")
                   (identity "/home/blaise/.ssh/id-guix-rsa")))))

(list earth-machine)
