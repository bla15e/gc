(define-module (bla15e machines titan)
  #:use-module (gnu)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)

  #:use-module (gnu system)

  #:use-module (bla15e systems)
  
  #:export (titan-system
	    titan-machine))


(define %pocket3-kernel-arguments
  (cons*
   ;; Rotate the Display from portrait to landscape
   "fbcon=rotate:1"
   "video=DSI-1:panel_orientation=right_side_up"
   ;; Suspend level S3 is not working, force to use S2
   "mem_sleep_default=s2idle"
   %default-kernel-arguments))

(define titan-system
  (operating-system
    (inherit bla15e-system)
    (host-name "titan")
    (kernel-arguments %pocket3-kernel-arguments)
    (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "1002-5F2D"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "aca7481d-3436-4b52-9c3c-919c199db7e4"
                                  'ext4))
                         (type "ext4")) %base-file-systems))
    (swap-devices (list (swap-space
                        (target (uuid
                                 "c548e73a-9a5c-4e52-a893-6b94c0a35e5f")))))))

(define titan-machine
  (machine
   (operating-system titan-system)
   (environment managed-host-environment-type)
   (configuration (machine-ssh-configuration
                   (host-name "192.168.1.252")	  
                   (system "x86_64-linux")
                   (user "root")
                   (identity "/home/blaise/.ssh/id-guix-rsa")))))

(list titan-machine)
