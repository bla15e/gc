(define-module (ise machines corp-ash-1)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (gnu system)
  
  #:use-module (ise system)
  #:use-module (ise system services))

(define corp-ash-1-system
  (operating-system
    (inherit (ise-managed-system))
    (host-name "corp-ash-1")))



