(define-module (ise infrastructure keys)
  #:use-module (guix gexp)
  #:export (
    ise-ssh-deploy
    ise-guix-substitute
    nonguix-substitute))

(define ise-ssh-deploy
  (plain-file "ise-deploy--key.pub"
	      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEP5/wYsmrslRaQiEWI5smrZA1umik2Mu4pk6tPn+GwGwOJZ7xi3uArYjLm02JnJdlQTaJexpkWHerq1UW5MTWgGnJC40pl5YP9LtShs2RkDyuvEm8fNDVc1VhVqLtlA4Oq5fHt//cZVIwEVNIYigYkeAO8k6UtwpdbmDIQMRsxbBj/NqyLCBpgeDEMgxsWwjt5qRQBw23ZZNAdMxh1yf7fj3Iqd3FMbKc57ejZvlpCPnSQ+CfIAKblwS0bV2O9XNH8BmVpgjrvTNpLVmetZGEtVUQPpviBYhyO9mXNtPDsLsBIgW+K26hJp7auQ9QWW33o9wtGZ1K1QT9lWjeFuCvu/XOz5IVgZnZRteIGz54TInWXS3vY6x2zqHhgiIm6BQTIiU8H81gMGkVx7AJUz4+T7Ecdyk/pZSr+gEhXODmlKFhKX5dpH1pL4CXZTTffTjDKzLDv/VYOl94JiMQ2RFkhBvTKtJPkPnSpRnyrVr92TMiuzrGwojC7V6XJLgfi3E= blaise@luna"))

(define ise-guix-substitute
  (plain-file "ise-substitute--key.pub"
  "(public-key
  (ecc
  (curve Ed25519)
  (q #858CB1A312C61EA14BD7ABA2369E36B5E60EDB885C02E2C0DD1C4C8F71236ECE#)))"))

(define nonguix-substitute 
  (plain-file "nonguix-substitute--key.pub"
                    "(public-key
  (ecc
    (curve Ed25519)
    (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
