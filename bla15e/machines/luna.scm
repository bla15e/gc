(define-module (bla15e machines luna)
  #:use-module (gnu)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (bla15e systems)
  #:export (luna-system
	    luna-machine))

(define luna-system
  (operating-system
    (inherit bla15e-system)
    (host-name "luna")
    (file-systems
     (cons*
      (file-system
	(mount-point "/")
	(device
	 (uuid "60c3a43d-fee2-4cb1-83af-3e35d34ea1e8"
               'ext4))
	(type "ext4"))
      (file-system
	(mount-point "/boot/efi")
	(device (uuid "C874-0521" 'fat32))
	(type "vfat"))
      %base-file-systems))))


(define luna-machine
  (machine
   (operating-system luna-system)
   (environment managed-host-environment-type)
   (configuration (machine-ssh-configuration
                   (host-name "192.168.1.227")
                   (system "x86_64-linux")
                   (user "root")
                   (identity "/home/blaise/.ssh/id-guix-rsa")))))
