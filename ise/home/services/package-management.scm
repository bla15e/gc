(define-module (ise home services package-management)
  #:use-module (gnu packages package-management)
  #:use-module (gnu home services)
  #:use-module (gnu home services utils)
  #:use-module (gnu services configuration)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:export (
            home-gwl-configuration
            home-gwl-service-type))

;; Service configuration for Guix workflow
;;
(define-configuration home-gwl-configuration
  (gwl-package
   (file-like gwl)
   "The Guix Workflow package to use"))

;; Set guix plugin path to find gwl
(define (add-gwl-envvars config)
  (let ((gwl-package (home-gwl-configuration-gwl-package config)))
    `(("GUIX_EXTENSIONS_PATH" . ,(file-append gwl-package "/share/guix/extensions")))))
;; install guix workflow package to the profile
(define (add-gwl-packages config)
  (list (home-gwl-configuration-gwl-package config)))

; Home service for guix workflow
; Installs the gwl package and sets the correct environment variable
(define home-gwl-service-type
  (service-type
   (name 'home-gwl)
   (extensions
    (list (service-extension home-environment-variables-service-type
                             add-gwl-envvars)
          (service-extension home-profile-service-type
                             add-gwl-packages)))

   (description "Home service that sets up the 'guix workflow' guix plugin")
   (default-value (home-gwl-configuration))))
