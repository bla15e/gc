(define-module (bla15e systems)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)

  #:use-module (guix gexp)

  #:use-module (gnu)
  
  #:use-module ((ise infrastructure keys) #:prefix key:)
  #:use-module (ise system services)

  #:use-module (gnu packages certs)
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages security-token)
  #:use-module (gnu packages wm)

  #:use-module (gnu services base)
  #:use-module (gnu services docker)
  #:use-module (gnu services desktop)
  #:use-module (gnu services networking)
  #:use-module (gnu services pm)
  #:use-module (gnu services security-token)
  #:use-module (gnu services sddm)
  #:use-module (gnu services ssh)
  #:use-module (gnu services xorg)

  #:use-module (gnu system setuid)

  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:export (bla15e-system))

(define %desktop-services-no-gdm
    (modify-services %desktop-services
      (delete gdm-service-type)))

(define bla15e-system
  (operating-system
    (kernel linux)
    (firmware (list linux-firmware))
    (initrd microcode-initrd)
    (locale "en_US.utf8")
    (timezone "America/New_York")
    (keyboard-layout (keyboard-layout "us"))
    (host-name "bla15e-change-me")
    (setuid-programs (cons*
		      (file-like->setuid-program
		       (file-append
			swaylock-effects
			"/bin/swaylock"))
		      %setuid-programs))

    ;; The list of user accounts ('root' is implicit).
    (users (cons* (user-account
                   (name "blaise")
                   (comment "Blaise")
                   (group "users")
                   (home-directory "/home/blaise")
                   (supplementary-groups '("wheel" "netdev" "audio" "video" "plugdev")))
                  %base-user-accounts))
    (packages (cons*
		     gnome-shell-extension-gsconnect
		     nss-certs
		     swaylock-effects
		     swayidle
		     sway
                     %base-packages))


    (services
     (cons* (service gnome-desktop-service-type)
	    (service tlp-service-type)
            (service sddm-service-type)
	    (service docker-service-type)
	    (service pcscd-service-type)
	    (bluetooth-service #:auto-enable? #f)
	    (udev-rules-service 'libu2f-host libu2f-host
				#:groups '("plugdev"))

            ;; This is the default list of services we
            ;; are constructing onto
            (managed-host-services key:ise-ssh-deploy key:ise-guix-substitute
				   #:base-services %desktop-services-no-gdm)))
    (bootloader (bootloader-configuration
		 (bootloader grub-efi-bootloader)
		 (targets (list "/boot/efi"))
		 (keyboard-layout keyboard-layout)))
    
    ;; The list of file systems that get "mounted".  The unique
    ;; file system identifiers there ("UUIDs") can be obtained
    ;; by running 'blkid' in a terminal.
    (file-systems (cons* (file-system
                           (mount-point "/boot/efi")
                           (device (uuid "D038-534A"
					 'fat32))
                           (type "vfat"))
			 (file-system
                           (mount-point "/")
                           (device (uuid
                                    "ae527b3e-a657-4366-91ae-f0cf652acac7"
                                    'ext4))
                           (type "ext4")) %base-file-systems))))


