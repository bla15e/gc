(define-module (ise system services)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (gnu services networking)
  #:use-module (gnu services base)
  #:use-module (gnu services ssh)

  #:use-module (gnu packages ssh)

  #:use-module ((ise infrastructure keys) #:prefix keys:)
  #:export (managed-host-services
            ise-managed-host-services))

;; Services for hosts managed by ise
(define* (ise-managed-host-services
          #:key
	  (ops-user #f)
          (base-services %base-services))
  (managed-host-services keys:ise-ssh-deploy keys:ise-guix-substitute
			 #:base-services base-services
			 #:ssh-authorized-keys
			 (if ops-user
			     `((,ops-user ,keys:ise-ssh-deploy))
			     `())))

;; Sets up services for a host thats managed remotely
(define* (managed-host-services ssh-key-deploy guix-key-substitute
				#:key
				;; TODO REPLACE!
				(ssh-authorized-keys `())
				(guix-authorized-keys %default-authorized-guix-keys)
		       (ssh-deploy-user "root")
		       (base-services %base-services))
  (cons*
   (service openssh-service-type
            (openssh-configuration
             (openssh openssh-sans-x)
	     (permit-root-login 'prohibit-password)
             (password-authentication? #f)
             (authorized-keys
	      (cons*
	       `(,ssh-deploy-user ,ssh-key-deploy)
	       ssh-authorized-keys))))
   (modify-services base-services
     (guix-service-type config =>
                        (guix-configuration
                         (inherit config)
		      	 (authorized-keys
                          (cons* guix-key-substitute
                          guix-authorized-keys)))))))
