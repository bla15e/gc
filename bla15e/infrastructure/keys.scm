(define-module (bla15e infrastructure keys)
  #:use-module (guix gexp)
  #:export (ssh-luna-rsa
	    ssh-luna-yubikey))

(define ssh-luna-rsa
  (plain-file "ssh-luna-rsa.pub"
	      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEP5/wYsmrslRaQiEWI5smrZA1umik2Mu4pk6tPn+GwGwOJZ7xi3uArYjLm02JnJdlQTaJexpkWHerq1UW5MTWgGnJC40pl5YP9LtShs2RkDyuvEm8fNDVc1VhVqLtlA4Oq5fHt//cZVIwEVNIYigYkeAO8k6UtwpdbmDIQMRsxbBj/NqyLCBpgeDEMgxsWwjt5qRQBw23ZZNAdMxh1yf7fj3Iqd3FMbKc57ejZvlpCPnSQ+CfIAKblwS0bV2O9XNH8BmVpgjrvTNpLVmetZGEtVUQPpviBYhyO9mXNtPDsLsBIgW+K26hJp7auQ9QWW33o9wtGZ1K1QT9lWjeFuCvu/XOz5IVgZnZRteIGz54TInWXS3vY6x2zqHhgiIm6BQTIiU8H81gMGkVx7AJUz4+T7Ecdyk/pZSr+gEhXODmlKFhKX5dpH1pL4CXZTTffTjDKzLDv/VYOl94JiMQ2RFkhBvTKtJPkPnSpRnyrVr92TMiuzrGwojC7V6XJLgfi3E= blaise@luna"))

(define ssh-luna-yubikey
  (plain-file "ssh-luna-ed25519_sk.pub"
	      "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIONaz34znFESWZfwLHvUvOtP6NQNewYJ43H+EncaTjxKAAAACnNzaDpibGFpc2U= blaise@luna"))


