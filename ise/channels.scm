(define-module (ise channels)
  #:use-module (guix channels))

(define nonguix
  (channel
   (name 'nonguix)
   (url "https://gitlab.com/nonguix/nonguix")
   (introduction
    (make-channel-introduction
     "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
     (openpgp-fingerprint
      "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5")))))

(define isecx
  (channel
   (name 'isecx)
   (branch "mainline")
   (url "https://gitlab.com/bla15e/guix-channel")
   (introduction
         (make-channel-introduction
          "1bfe175f57d8c48a553301d3b57313fcf295e982"
          (openpgp-fingerprint
           "24EB EAE6 7E7B 2738 606D  4FAA 71CE FF40 28E1 5BE4")))))
