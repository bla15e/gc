(define-module (ise machines corp-hel-1)
  #:use-module (gnu)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (ise system))

(define corp-hel-1-system
  (operating-system
    (inherit (ise-managed-system))
    (host-name "corp-hel-1")))

(list (machine
  (operating-system corp-hel-1-system)
  (environment managed-host-environment-type)
  (configuration (machine-ssh-configuration
		  (host-name "corp-hel-1.ise.international")
		  (system "x86_64-linux")
		  (user "root")
		  (identity "/home/blaise/.ssh/id-guix-rsa")))))
