(define-module (ise home services wm)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (rde home services wm))

(define* (waybar-module
          name
          #:optional
          (config '())
          (style '())
          #:key
          (placement 'modules-right)
          (bar-id 'main))
  "Returns a service, which extends home-waybar-service-type in the way the
module will be added to the BAR-ID."
  (simple-service
   (symbol-append 'waybar-module- name)
   home-waybar-service-type
   (home-waybar-extension
    (config `#(((name . ,bar-id)
                (,placement . #(,name))
                (,name . ,config))))
    (style-css style))))


