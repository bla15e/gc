(define-module (bla15e machines mimas)
  #:use-module (gnu)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)

  #:use-module (bla15e systems)
  
  #:export (mimas-system
	    mimas-machine))

(define mimas-system
  (operating-system
    (inherit bla15e-system)
    (host-name "mimas")
    (swap-devices (list (swap-space
			 (target (uuid
                                  "5b2982b3-57f1-417d-ac65-563965320b60")))))
    (file-systems (cons* (file-system
                           (mount-point "/boot/efi")
                           (device (uuid "D038-534A"
					 'fat32))
                           (type "vfat"))
			 (file-system
                           (mount-point "/")
                           (device (uuid
                                    "ae527b3e-a657-4366-91ae-f0cf652acac7"
                                    'ext4))
                           (type "ext4")) %base-file-systems))))

(define mimas-machine
  (machine
   (operating-system mimas-system)
   (environment managed-host-environment-type)
   (configuration (machine-ssh-configuration
                   (host-name "192.168.1.227")
		   (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJBp37k4rSHWazzKtqB2zmqWx9kLxAdGX5B9gNhA1Vef root@(none)")
                   (system "x86_64-linux")
                   (user "root")
                   (identity "/home/blaise/.ssh/id-guix-rsa")))))

(list mimas-machine)
