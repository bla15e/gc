(define-module (bla15e home)
  #:use-module (gnu home)
  #:use-module (gnu packages)
  #:use-module (gnu home services)

  #:use-module (ise home services emacs)
  #:use-module (ise home services package-management)

  #:use-module (bla15e home services)
  #:export (bla15e-home-env))

(define %bla15e-packages
  (specifications->packages
   (list
    "xdg-utils"
    "obs-wlrobs"
    "obs"
    "grim"
    "wl-clipboard"
    "brightnessctl"
    "nss-certs"

    ;; Sway/Wayland Tools
    "wlr-randr"
    ;; Development Tools
    "xdot"
    "iputils"
    "bpytop"
    ;; Media
    "ffmpeg"
    "yt-dlp"
    "playerctl"
    "lollypop"
    "mpd"
    "mpv"
    "blueman"
    "git"
    "rofi"

    ;; Fonts
    "fontconfig"
    "font-wqy-zenhei"
    "font-dejavu"
    ;; Programming Font
    "font-iosevka"

    "alacritty"
    "bat"
    "mpv"
    "xdg-desktop-portal"
    "curl"
    "pcsc-lite"
    "eom"
    "xdg-desktop-portal-wlr")))

(define bla15e-home-env
  (home-environment
   (packages %bla15e-packages)
   (services
    (list
     home-isemacs-service
     service-openssh
     service-sway
     (service home-gwl-service-type)
     service-bash))))
