(define-module (bla15e home services)
  #:use-module (gnu home services)
  #:use-module (gnu home services utils)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services ssh)

  #:use-module (rde home services wm)
  
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu services configuration)

  #:use-module (bla15e home dotfiles)
  #:use-module (bla15e infrastructure keys)
  #:use-module (guix packages)
  #:use-module (guix gexp)

  #:export (service-bash
	    service-sway
	    service-openssh))

(define service-bash
  (service home-bash-service-type
           (home-bash-configuration
            (aliases
             '(("grep" . "grep --color=auto")
               ("ll" . "ls -l")
               ("ls" . "ls -p --color=auto")))
            (bashrc
             (list config-bashrc))
            (environment-variables
             '(("EDITOR" . "emacsclient -a ''")
              ; ("GUIX_LOCPATH" . "$HOME/.guix-profile/lib/locale")
              ; ("GUIX_EXTRA_PROFILES" . "$HOME/.guix-extra-profiles")
               ; ("SSH_AUTH_SOCK" . "$XDG_RUNTIME_DIR/ssh-agent.socket")
                ("SSL_CERT_DIR" . "$HOME/.guix-home/profile/etc/ssl/certs")
                ("SSL_CERT_FILE" . "$HOME/.guix-home/profile/etc/ssl/certs/ca-certificates.crt")
                ("GIT_SSL_CAINFO" . "$SSL_CERT_FILE"))))))

(define service-openssh
  (service home-openssh-service-type
	   (home-openssh-configuration
	    (authorized-keys (list ssh-luna-rsa
				   ssh-luna-yubikey)))))

(define service-sway
  (service home-sway-service-type
	   (home-sway-configuration
	    (config config-sway))))


