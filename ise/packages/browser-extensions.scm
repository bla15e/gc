(define-module (ise packages browser-extensions)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu build chromium-extension)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python))

(define vimium
  (package
    (name "vimium")
    (version "1.67.3")
    (home-page "https://github.com/philc/vimium")
    (source (origin
              (method git-fetch)
              (uri (git-reference (url home-page) (commit (string-append "v." version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15vrwkxs9792fcfxa74m822mh22xaijcw0j254z61llf022agmg0"))))
    (build-system copy-build-system)
    (synopsis "The Hacker's Browser")
    (description
     "Vimium is a browser extension that provides keyboard-based navigation and control of the web in the spirit of the Vim editor.")
    (license license:expat)))

(define butterchurn
  (package
    (name "butterchurn")
    (version "1.0.0")
    (home-page "https://github.com/jberg/butterchurn-chrome-extension")
    (source (origin
              (method git-fetch)
              (uri (git-reference (url home-page) (commit "793c654de34e2f6ebe74fb7dcb6ed137fd126d59")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "037axy11k5ihfs08593jkv40n3rjfjd2r1abdi2rcdx65w94lwz9"))))
    (build-system copy-build-system)
    (synopsis "The Hacker's Browser")
    (description
     "Vimium is a browser extension that provides keyboard-based navigation and control of the web in the spirit of the Vim editor.")
    (license license:expat)))

(define-public vimium/chromium
  (make-chromium-extension vimium))

(define-public butterchurn/chromium
  (make-chromium-extension butterchurn))

butterchurn/chromium
