(define-module (ise system services docker-compose)
  #:use-module (guix modules)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services base)
  #:use-module (gnu services docker)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages docker)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:export (docker-compose-configuration
            docker-compose-service-type))

(define-configuration compose-up-configuration
  (compose-package
   (file-like docker-compose)
   "The docker-compose package to use")
  (file 
   (file-like docker)
   "File that 'docker-compose' will be ran on '--file")
  (verbose?
   (boolean #f)
   "Show more output --verbose")
  (debug?
   (boolean #f)
   "Set log level for debugging, --log-level DEBUG")
  (force-recreate?
   (boolean #f)
   "Recreate containers even if their configuration and image haven't change")
  (no-serialization))


(define-configuration docker-volume-configuration
  (driver
   (string "local")
   "A simple document")
  (opts
   (list `())
   "option arguments")
  (no-serialization))


;; TODO: Make servicble
;; Propose: cx-compose
;; Root service creates volumes, networks,
;; can be extended to run more compose-files?

;; Docker Host Configuration?
;; Creates networks and volumes

(define (compose-up-shepherd-service config)
  (let* ((compose-binary (string-append
                          (compose-up-configuration-compose-package config) "/bin/docker-compose"))
         (file-arg (string-append "--file=" (compose-up-configuration-file config))) 
         (verbose? (compose-up-configuration-verbose? config))
         (debug-arg (if (compose-up-configuration-debug? config)
                          '("--log-level=debug")
                          '()))
         (force-recreate? (compose-up-configuration-force-recreate? config)))
    (shepherd-service
     (documentation "Runs 'docker-compose up'")
     (provision '(compose-up))
     (requirement '(dockerd))
     (start #~(make-forkexec-constructor
               (list #$compose-binary 
                     #$file-arg
                     #$@debug-arg
                     "up")))
     (stop #~(make-fork-exec-constructor
              (list #$compose-binary
                    #$file-arg
                    #$@debug-arg
                    "down"))))))

;; Compose your host specifically
;; Set up your docker volumes, 
(define compose-host-service-type
  (service-type (name 'docker-compose)
                (description
                 "Use host to run services modeled in docker-compose files. Can be extended")
                (extensions
                 ;; Run 'docker-compose' as a root service
                 (list (service-extension shepherd-root-service-type
                                          compose-up-shepherd-service)))
                (default-value (compose-up-configuration))))



(define docker-compose-service-type
  (service-type (name 'docker-compose)
                (description
                 "Use host to run services modeled in docker-compose files. Can be extended")
                (extensions
                 ;; Run 'docker-compose' as a root service
                 (list (service-extension shepherd-root-service-type
                                          compose-up-shepherd-service)))
                (default-value (compose-up-configuration))))


(define docker-compose-project
  (local-file "./../../packages/src/gitea/" #:recursive? #t))

