(define-module (ise packages avax)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system go)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages base)
  #:use-module (guix build-system trivial)

  #:use-module (cx build-system avax)
  #:export (avalanchego
	    avalanchego-node-old
            go-github-com-ava-labs-coreth
            go-github-com-ava-labs-avalanchego))

(define-public go-github-com-supranational-blst
  (package
    (name "go-github-com-supranational-blst")
    (version "0.3.10")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/supranational/blst")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "01m28xxmm1x7riwyax7v9rycwl5isi06h2b2hl4gxnnylkayisn5"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/supranational/blst"))
    (home-page "https://github.com/supranational/blst")
    (synopsis "blst")
    (description
     "blst (pronounced blast') is a BLS12-381 signature library focused on performance
and security.  It is written in C and assembly.")
    (license license:asl2.0)))

(define go-github-com-ava-labs-coreth
  (package
    (name "go-github-com-ava-labs-coreth")
    (license license:bsd-3)
    (version "0.11.0")
    (source (origin
             (method url-fetch)
             (uri "https://usc1.contabostorage.com/5e37e084f153485691f07db407003650:cx-src/coreth-0.11.0.tar.gz")
             (sha256
              (base32
               "0v5jnvxn2frdpagqg8xvp74vbkp9bkkxcgr59myfqjmzrm50y1yr"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/ava-labs/coreth/plugin"
       #:unpack-path "github.com/ava-labs/coreth"
       #:go ,go-1.19
       #:phases (modify-phases %standard-phases
                  (add-after 'build 'rename-binary
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (plugin-binary (string-append out "/bin/plugin"))
                             (evm-plugin (string-append out "/bin/evm")))
                        (rename-file plugin-binary evm-plugin)
                        #t))))))
    ;; TODO Handle go dependencies in Guix
    (home-page "https://;github.com/ava-labs/coreth")
    (synopsis "Coreth and the C-Chain")
    (description
     "@url{https://docs.avax.network/learn/platform-overview,Avalanche} is a network
composed of multiple blockchains.  Each blockchain is an instance of a Virtual
Machine (VM), much like an object in an object-oriented language is an instance
of a class.  That is, the VM defines the behavior of the blockchain.  Coreth
(from core Ethereum) is thegfgfvgfvgfcbfgcfvgvgfbc
@url{https://docs.avax.network/learn/platform-overvivgfvvvfgvew#virtual-machines,Virtual
Machine (VM)} that defines the Contract Chain (C-Chain).  This chain implements
the Ethereum Virtual Machine and supports Solidity smart contracts as well as
most other Ethereum client functionality.")))

;; TODO source-code for 
(define go-github-com-ava-labs-subnet-evm
  (package
    (inherit go-github-com-ava-labs-coreth)
    (name "go-github-com-ava-labs-subnet-evm")
    (synopsis "Subnet EVM")
    (description
     "@url{https://docs.avax.network/overview/getting-started/avalanche-platform,Avalanche}
is a network composed of multiple blockchains.  Each blockchain is an instance
of a Virtual Machine (VM), much like an object in an object-oriented language is
an instance of a class.  That is, the VM defines the behavior of the blockchain.")))
;; TODO source-code for 
(define go-github-com-ava-labs-timestampvm
  (package
    (inherit go-github-com-ava-labs-coreth)
    (name "go-github-com-ava-labs-timestampvm")
    (synopsis "Timestamp Virtual Machine")
    (description "Requires Go version >= 1.17.9")))



(define go-github-com-ava-labs-avalanchego
  (package
    (name "go-github-com-ava-labs-avalanchego")
    (version "1.9.0")
    (license license:bsd-3)
    (source (origin
             (method url-fetch)
             (uri "https://usc1.contabostorage.com/5e37e084f153485691f07db407003650:cx-src/avalanchego-1.9.0.tar.gz")
             (sha256
              (base32
               "0zjjg6j68czzmnnnczigdnybxa04sfmkg6c78bfbxcvq1ifpyfnd"))))
    (build-system go-build-system)
    (arguments
     `(#:go ,go-1.19
       #:import-path "github.com/ava-labs/avalanchego/main"
       #:unpack-path "github.com/ava-labs/avalanchego"
       #:install-source? #f
       #:phases (modify-phases %standard-phases
                  (add-after 'build 'rename-binary
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (main-binary (string-append out "/bin/main"))
                             (avalanchego-binary (string-append out "/bin/avalanchego")))
                        ;; Rename the binary from "main" to "avalanchego"
                        (rename-file main-binary avalanchego-binary)
                        #t))))))
    (home-page "https://github.com/ava-labs/avalanchego")
    (synopsis "Installation")
    (description
     "Node implementation for the @url{https://avax.network,Avalanche} network - a
blockchains platform with high throughput, and blazing fast transactions.")))


(define avalanchego
  (package
    (inherit go-github-com-ava-labs-avalanchego)
    (source #f)
    (name "avalanchego-node")
    (version "1.7.14")
    (home-page "https://avax.com")
    (build-system avax-node-build-system)))
;; TODO Abstract into `avax-node-build-system'
;; Takes in a map like {
;;  avalanchego: (default)
;;  plugins: list(packages)
;;
;;}
(define avalanchego-node-old
  (package
    (source #f)
    (name "avalanchego-old")
    (license license:bsd-3)
    (version "1.7.14")
    (home-page "https://package.ise.ninja")
    ;; To build, we want to copy all the plugin-name
    (build-system trivial-build-system)
    (arguments
     '(
       #:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (inputs (assoc-ref %build-inputs "inputs"))
                (avalanchego (string-append (assoc-ref %build-inputs "go-github-com-ava-labs-avalanchego")
                                            "/bin/avalanchego"))
                (evm-plugin (string-append (assoc-ref %build-inputs "go-github-com-ava-labs-coreth")
                                           "/bin/evm")))
           (install-file avalanchego (string-append out "/bin"))
           (install-file evm-plugin (string-append out "/plugins"))
           #t))))
    (inputs (list go-github-com-ava-labs-avalanchego go-github-com-ava-labs-coreth))
    (synopsis "Avalanche node with evm plugin")
    (description "Avalanchego node running with ava-labs coreth evm plugin")))
