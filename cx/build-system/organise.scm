(define-module (cx build-system organise)
  #:use-module (guix gexp)
  #:use-module (guix build-system)
  #:use-module (guix build-system emacs)
  #:export (organise-build-system))

(define organise-build-system
  (build-system
    (name 'organise)
    (description "The build system for organise projects")
    (lower (build-system-lower emacs-build-system))))
