(define-module (bla15e home dotfiles)
  #:use-module (guix gexp)
  #:export(config-sway
		   config-bashrc))


(define (swaylock-cmd)
  "swaylock \
	--screenshots \
	--clock \
	--indicator \
	--indicator-radius 100 \
	--indicator-thickness 7 \
	--effect-blur 7x5 \
	--effect-vignette 0.5:0.5 \
	--ring-color bb00cc \
	--key-hl-color 880033 \
	--line-color 00000000 \
	--inside-color 00000088 \
	--separator-color 00000000 \
	--grace 2 \
	--fade-in 0.2")

(define config-sway
  `((include ,(local-file  "config/sway"))
    (output * bg ,(local-file "bg/moon.jpg") fill)
    (bindsym $mod+shift+p exec ,(swaylock-cmd))))

(define config-bashrc
   (local-file "config/bashrc"))
