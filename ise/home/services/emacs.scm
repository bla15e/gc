(define-module (ise home services emacs)
  #:use-module (gnu home services)
  #:use-module (gnu home services utils)
  #:use-module (gnu home services shepherd)

  #:use-module (gnu home-services emacs)

  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)

  #:use-module (guix packages)
  #:use-module (guix gexp)

  #:export (home-isemacs-service))

(define %isemacs-elisp-packages
  (map specification->package
       (list
	;; icons
	"emacs-all-the-icons"
	"emacs-all-the-icons-completion"
	;; treemacs
	"emacs-treemacs"
	"emacs-treemacs-extra"

	;; Basic Tools
        "emacs-use-package"
	"emacs-orderless"

	;; Auto complete
	"emacs-corfu"

	;; Geographic Utilities
	"emacs-osm"

	"emacs-dashboard"
	"emacs-vertico"
        "emacs-orderless"
        "emacs-marginalia"
         "emacs-consult"
         "emacs-embark"
	 "emacs-vterm"
					;"emacs-corfu"
	 "emacs-flycheck"
         "emacs-cape"
         "emacs-which-key"
         "emacs-projectile"
	 "emacs-multiple-cursors"
         ;"emacs-popper"
         "emacs-consult-dir"
         ;"emacs-direnv"
         ;"emacs-avy"
         "emacs-embark"
         ;"emacs-unkillable-scratch"
         ;"emacs-wgrep"
        ; "emacs-jupyter"
         ;"emacs-frames-only-mode"
         ;; Tool Modes
         "emacs-magit"
         "emacs-magit-todos"
         "emacs-guix"
         "emacs-vterm"
         "emacs-pdf-tools"
         
         ;"emacs-notmuch"
         ;Language Tools
         "emacs-markdown-mode"
         "emacs-ledger-mode"
         "emacs-yaml-mode"
	 "emacs-dotenv-mode"
	 ;;lisp tools
	 "emacs-paredit"
         "emacs-rainbow-delimiters"
         "emacs-rainbow-blocks"
         ;;scheme tools
         "emacs-geiser"
         "emacs-geiser-guile"
	 "emacs-flycheck-guile"
         
	 "emacs-haskell-mode"
         "emacs-dante"
         "emacs-clojure-mode"
         "emacs-cider"
         "emacs-rust-mode"
         "emacs-macrostep"
         "emacs-web-mode"
         "emacs-js2-mode"
         "emacs-typescript-mode"
         "emacs-add-node-modules-path"
         "emacs-prettier"
         "emacs-graphviz-dot-mode"
         ;; General Editing Modes
         "emacs-sudo-edit"
         "emacs-expand-region"
         "emacs-multiple-cursors"
         "emacs-phi-search"
         "emacs-ws-butler"
         "emacs-yasnippet"
         "emacs-yasnippet-snippets"
         "emacs-ivy-yasnippet"
         "emacs-flyspell-correct"
         ;; Org
         "emacs-org-modern"
         "emacs-org-journal"
         "emacs-org-roam"
         "emacs-org-download"
         "emacs-org-contrib"
         "emacs-org-fragtog"
	 "emacs-org-present"

	 ;; Useful with org-present
	 "emacs-logos"
	 "emacs-olivetti"

	 ;; Convert buffer text and decorations into html
	 "emacs-htmlize"
	 
         "emacs-ample-theme"
         ;; Appearance
         "emacs-spacemacs-theme"
         "emacs-nord-theme"
         "emacs-olivetti"
	 "emacs-moody")))

(define isemacs-early-init-el
  '(;; Early init file is for configuration before the frame's GUI and
    ;; package.el are initialized.

    ;; Disable the menu-bar, tool-bar, and window fringes
    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    
    (set-fringe-mode 0)))

(define isemacs-init-el
  '(
    (setq warning-minimum-level :error)
    (setq backup-directory-alist `(("." . "~/.isemacs/backups")))
    ;; TODO
    (setq custom-file "~/.isemacs/emacs-custom.el")
    (load custom-file t)
    ;; Vanilla Keybinds
    (global-set-key (kbd "C-x k") 'kill-current-buffer)
    (setq initial-major-mode 'scheme-mode)
    (setq initial-scratch-message ";; Scratch \n")



    (setq inhibit-startup-screen t)
    
    (require 'dashboard)
    (require 'rainbow-delimiters)
    (require 'rainbow-blocks)

    (dashboard-setup-startup-hook)
    (setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))


    ;; Set the title
    (setq dashboard-banner-logo-title "Welcome to Isemacs!")
    ;; Set the banner
    (setq dashboard-startup-banner 'logo)


    ;; Disable the footer
    (setq dashboard-set-footer nil)
    ;; Value can be
    ;; 'official which displays the official emacs logo
    ;; 'logo which displays an alternative emacs logo
    ;; 1, 2 or 3 which displays one of the text banners
    ;; "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer

    ;; Content is not centered by default. To center, set
    (setq dashboard-center-content t)


					;(setq-default indent-tabs-mode nil)

    (require 'dotenv-mode) ; unless installed from a package
    (add-to-list 'auto-mode-alist '("\\.env\\..*\\'" . dotenv-mode)) ;; for optionally supporting additional file extensions such as `.env.test' with this major mode

    ;; Configure Emacs Packages
    (use-package corfu
		 ;; Optional customizations
		 :custom
		 ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
		 (corfu-auto t)                 ;; Enable auto completion
		 ;; (corfu-separator ?\s)          ;; Orderless field separator
		 ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
		 ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
		 ;; (corfu-preview-current nil)    ;; Disable current candidate preview
		 ;; (corfu-preselect-first nil)    ;; Disable candidate preselection
		 ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
		 ;; (corfu-echo-documentation nil) ;; Disable documentation in the echo area
		 ;; (corfu-scroll-margin 5)        ;; Use scroll margin

		 ;; Enable Corfu only for certain modes.
		 ;; :hook ((prog-mode . corfu-mode)
		 ;;        (shell-mode . corfu-mode)
		 ;;        (eshell-mode . corfu-mode))

		 ;; Recommended: Enable Corfu globally.
		 ;; This is recommended since Dabbrev can be used globally (M-/).
		 ;; See also `corfu-excluded-modes'.
		 :init
		 (global-corfu-mode))

    ;; A few more useful configurations...
    (use-package emacs
		 :init
		 ;; TAB cycle if there are only few candidates
		 (setq completion-cycle-threshold 3)

		 ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
		 ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
		 ;; (setq read-extended-command-predicate
		 ;;       #'command-completion-default-include-p)

		 ;; Enable indentation+completion using the TAB key.
		 ;; `completion-at-point' is often bound to M-TAB.
		 (setq tab-always-indent 'complete))


    ;; Org Mode COnfiguration
    (use-package org
		 :mode (("\\.org$" . org-mode))
		 :custom
		 (org-support-shift-select t)
		 :bind
		 (:map global-map
		  ("C-c a" . org-agenda)
		  ("C-c l" . org-store-link)
		  ("C-c c" . org-capture)))

    ;; Autoload
    (autoload 'org-present "org-present" nil t)


    (use-package orderless
		 :ensure t
		 :custom
		 (completion-styles '(orderless basic))
		 (completion-category-overrides '((file (styles basic partial-completion)))))

    (use-package magit
		 :bind
		 ("C-x g" . magit-status))

    (use-package magit-todos
		 :config
		 (magit-todos-mode))

    (require 'cfrs)
    (use-package treemacs
		 :ensure t
		 :defer t
		 :bind
		 (:map global-map
		  ("M-0"       . treemacs-select-window)
		  ("C-x t 1"   . treemacs-delete-other-windows)
		  ("C-x t t"   . treemacs)
		  ("C-x t d"   . treemacs-select-directory)
		  ("C-x t B"   . treemacs-bookmark)
		  ("C-x t C-t" . treemacs-find-file)
		  ("C-x t M-t" . treemacs-find-tag)))

    
    (use-package consult-dir
		 :bind (("C-x C-d" . consult-dir)
			:map minibuffer-local-completion-map
			("C-x C-d" . consult-dir))
		 :config
		 (setq consult-dir-project-list-function #'consult-dir-projectile-dirs))

    (use-package guix-popup
		 :init
		 (global-guix-prettify-mode 1)
		 :hook
		 (scheme-mode . guix-devel-mode)
		 :bind
		 ("C-x y" . guix-popup))

    (use-package vterm
		 :config
		 (setq vterm-timer-delay 0.01))

    (use-package expand-region
		 :bind
		 ("C-=" . er/expand-region))

    (use-package multiple-cursors
		 :bind
		 ("C-S-c C-S-c" . mc/edit-lines)
		 ("C->" . mc/mark-next-like-this)
		 ("C-<" . mc/mark-previous-like-this)
		 ("C-c C-<" . mc/mark-all-like-this))

    (use-package flycheck
		 :ensure t
		 :init (global-flycheck-mode))
    (require 'flycheck-guile)

    (use-package embark
		 :bind
		 (("C-." . embark-act)         ;; pick some comfortable binding
		  ("C-;" . embark-dwim)        ;; good alternative: M-.
		  ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
		 :init
		 ;; Optionally replace the key help with a completing-read interface
		 ;; (setq prefix-help-command #'embark-prefix-help-command)
		 :config
		 ;; Hide the mode line of the Embark live/completions buffers
		 ;; (add-to-list 'display-buffer-alist
		 ;;              '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
		 ;;                nil
		 ;;                (window-parameters (mode-line-format . none))))
		 :custom
		 ;; Circumvents an error when using frames-only-mode
		 (embark-verbose-indicator-display-action
		  '(display-buffer-in-side-window (side . right))))

    (use-package graphviz-dot-mode
		 :config
		 (setq graphviz-dot-indent-width 4))
    
    (use-package which-key
		 :init
		 (which-key-mode 1))

    ;; Project configuration
    (use-package projectile
		 :bind-keymap
		 ("C-c p" . projectile-command-map)
		 :config
		 (require 'subr-x)
		 (projectile-mode +1))

    (use-package dired
		 :config
		 (setq dired-listing-switches "-lahv --group-directories-first"
		       dired-auto-revert-buffer t
		       dired-dwim-target t)
		 (add-hook 'dired-mode-hook 'dired-hide-details-mode))

    (use-package yasnippet
		 :config
		 (yas-global-mode 1))

    (use-package osm
		 :bind (("C-c m h" . osm-home)
			("C-c m s" . osm-search)
			("C-c m v" . osm-server)
			("C-c m t" . osm-goto)
			("C-c m x" . osm-gpx-show)
			("C-c m j" . osm-bookmark-jump))

		 :custom
		 ;; Take a look at the customization group `osm' for more options.
		 (osm-server 'default) ;; Configure the tile server
		 (osm-copyright t)     ;; Display the copyright information

		 :init
		 ;; Load Org link support
		 (with-eval-after-load 'org
				       (require 'osm-ol))) 
   
    (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)

    
    (add-hook 'emacs-lisp-mode-hook       'enable-paredit-mode)
    (add-hook 'eval-expression-minibuffer-setup-hook 'enable-paredit-mode)
    (add-hook 'ielm-mode-hook             'enable-paredit-mode)
    (add-hook 'lisp-mode-hook             'enable-paredit-mode)
    (add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
    (add-hook 'scheme-mode-hook           'enable-paredit-mode)

    (add-hook 'org-mode-mode-hook 'olivetti-mode)
    (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'scheme-mode-hook 'hs-minor-mode)
    

    
    ;; Add in ISE code
    (with-eval-after-load 'geiser-guile
			  (setq geiser-guile-load-path
				(append (with-temp-buffer
					 (insert "'")
					 (call-process-shell-command
					  "echo '(@ (gnu packages) %package-module-path) (write %load-path) (newline)' | guix repl -- | tail -n 2 | head -n 1"
					  nil t)
					 (eval-last-sexp t))
					geiser-guile-load-path)))

    ;; Appearance
    (setq-default truncate-lines t)
    (setq-default fill-column 80)

    (use-package all-the-icons
		 :if (display-graphic-p))
    (all-the-icons-completion-mode)
    (require 'treemacs-all-the-icons)
    (treemacs-load-theme "all-the-icons")

    
    (use-package spacemacs-theme
                 :defer t
                :init (load-theme 'spacemacs-dark t)
                 :config
                 ;; Do not use a different background color for comments.
                 (setq spacemacs-theme-comment-bg nil)
                 ;; Comments should appear in italics.
                 (setq spacemacs-theme-comment-italic t))
                
                  (add-hook 'after-make-frame-functions
                  (lambda (&optional frame)
                      (when frame
                        (select-frame frame))
                      (load-theme 'spacemacs-dark t)))
        
    (use-package moody
		 :config
		 (setq x-underline-at-descent-line t)
		 (moody-replace-mode-line-buffer-identification)
		 (moody-replace-vc-mode))))
(define (add-isemacs-packages config)
  (list ripgrep))

;; TODO Fix and make it work
(define home-isemacs-service-type
  (service-type
   (name 'home-isemacs)
   (description "emacs service for isemacs")
   (extensions (service-type-extensions home-emacs-service-type))
   (default-value (service-type-default-value home-emacs-service-type))))

(define home-isemacs-service
  (service home-emacs-service-type
           (home-emacs-configuration
	    (package emacs)
            (elisp-packages %isemacs-elisp-packages)
	    (early-init-el isemacs-early-init-el)
	    (init-el isemacs-init-el))))
