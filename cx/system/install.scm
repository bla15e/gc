(define-module (cx system install)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages package-management)

  #:use-module (nongnu packages linux)

  #:use-module (ise channels)

  #:export (cx-installation-os))


;; TODO Update installer to enable selection of channels
;; TODO Update installer to display qr-code ?
;; Some kind of mechanism, insert installer
;; present qr-code
;; configure as needed
;; or maybe just present preconfigured systems defined in our channels?

(define %guix-channels
  (scheme-file
   "channels.scm"
   #~(cons* (channel
             (name 'nonguix)
             (url "https://gitlab.com/nonguix/nonguix")
             (introduction
              (make-channel-introduction
               "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
               (openpgp-fingerprint
                "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
            %default-channels)))

;; The cx-installation os overrides the default guix installation os with cx goodies
;; Currently, we use the non-free linux kernel and firmware
(define cx-installation-os
  (operating-system
   (inherit installation-os)
   (kernel linux)
   (firmware (list linux-firmware))

   (services
    (cons*
     ;; This should configure guix to use the channel file during installation
     (extra-special-file "/etc/guix/channels.scm" %guix-channels)
     (operating-system-user-services installation-os)))

   (packages
    (cons*
     git vim
     (operating-system-packages installation-os)))))


   
   
