(define-module (ise machines)
  #:use-module (gnu)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)

  #:use-module (ise system)
  #:export (ish-ssh-vm))


(define (ise-ssh-vm host-name)
  (machine
   (operating-system (ise-managed-system))
   (environment managed-host-environment-type)
   (configuration (machine-ssh-configuration
		   (host-name host-name)
		   (system "x86_64-linux")
		   (user "root")
		   (identity "/home/blaise/.ssh/id-guix-rsa")))))
