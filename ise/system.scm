(define-module (ise system)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (gnu services networking)
  #:use-module (gnu services base)
  #:use-module (gnu services ssh)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages tls)

  #:use-module (ise system services)
  #:use-module ((ise infrastructure keys) #:prefix keys:)

  #:export (ise-managed-system))

;System configuration used to bootstrap guix instances on iws

;; Can we get rid of these
(define %vm-initrd-modules
  (cons* "virtio_scsi"
         %base-initrd-modules))
;; Can we get rid of these
(define bootloader-sda-grub
  (bootloader-configuration
   (bootloader grub-bootloader)
   (targets '("/dev/sda"))))

;; Do we really need firmware?
(define* (ise-managed-system 
			  #:key
			  (ops-user "iseadmin")
			  ;; if deployment-user != root
			  (locale "en_US.utf8")
			  (timezone "Etc/UTC"))
  (operating-system
    (host-name "give-me-a-hostname")
    (timezone timezone)
    (locale locale)

    (initrd-modules %vm-initrd-modules)
    (bootloader bootloader-sda-grub)
    (file-systems (cons* (file-system
                           (device "/dev/sda1")
                           (mount-point "/")
                           (type "ext4"))
                         %base-file-systems))

    ;; Keep this around for now
    ;; Update to createa  'deployment user' if necessary
    ;; Will have to update sudoers to allow the deployment user to invoke sudo without passwd
    (users (cons (user-account
                  (name ops-user)
                  (comment ops-user)
		  (home-directory (string-append "/home/" ops-user))
                  (group "users")
                  (supplementary-groups '("wheel")))
                 %base-user-accounts))

    ;; Globally-installed packages.
    (packages (cons* screen nss-certs gnutls %base-packages))

    ;; iseadmin user needs to be able to use 'sudo' without password for 'guix deploy'
    ;; TODO add approval for guix deploy key
   
    ;; 
    (services (cons* (service dhcp-client-service-type)
		     (ise-managed-host-services
		      #:ops-user ops-user)))))

