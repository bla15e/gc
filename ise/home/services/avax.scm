(define-module (ise home services avax)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (gnu home services utils)
  #:use-module (gnu services configuration)

  #:use-module (ise packages avax)
  #:export (home-avax-configuration
	    home-avax-service-type))


(define %default-plugins
  '(("evm"  ,(file-append go-github-com-ava-labs-coreth "/bin/evm"))))
;; Simple configuration
(define-configuration home-avax-configuration
  (avalanchego
   (file-like go-github-com-ava-labs-avalanchego)
   "Avalanchego package to use")
  ;; pairs of (plugin-name . (file-like plugin-binary))
  (plugins
   (alist %default-plugins)
   "Association list of plugin names and binaries. Populates
@file{~/.avalanchego/plugins}")
  (node-config
   (file-like go-github-com-ava-labs-avalanchego)
   "Saved to ~/.avalanchego/configs/node.json")
  ;; Pairs of (chain_id . (file-like config-file-json))
  (chains
   (alist  '())
   "Populates ~/.avalanchego/configs/chains")
  ;; Pairs of (subnet_id . (file-like subnet-config-json))
  (subnets
   (alist  '())
   "Populates ~/.avalanchego/configs/subnets")
  (no-serialization))

(define (avax-packages config)
  (list (home-avax-configuration-avalanchego config)))

(define (avax-configuration-files config)
  `())
(define home-avax-service-type
  (service-type
   (name 'home-avax)
   (extensions
    (list
     (service-extension home-profile-service-type
			avax-packages)))
   (description "Configure your avalanchego node")
   (default-value (home-avax-configuration))))
