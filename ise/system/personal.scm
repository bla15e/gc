(define-module (ise system personal)
  #:use-module (gnu services base)
  #:use-module (gnu services desktop)
  #:use-module (gnu services docker)
  #:use-module (gnu services security-token)
  #:use-module (gnu services networking)
  #:use-module (gnu services sddm)
  #:use-module (gnu services ssh)
  #:use-module (gnu services sddm)
  #:use-module (gnu services pm)
  #:use-module (gnu services xorg)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages security-token)
  #:use-module (gnu packages docker)
  #:use-module (guix gexp)
  #:use-module (guix channels)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:export (%pc-services
            %laptop-services
            %hacker-services
            %pc-packages
            %hacker-packages))
(define %pc-services
  ; Append a list of services to add to our modified desktop services
  (append
   (list
    (service sddm-service-type)
    (screen-locker-service swaylock)
    (service pcscd-service-type))
   (modify-services %desktop-services
                    (guix-service-type config =>
                                       (guix-configuration
                                        (inherit config)
                                        (substitute-urls
                                         (append (list "https://substitutes.nonguix.org")
                                                 %default-substitute-urls))
                                        (authorized-keys
                                         (append (list (plain-file "non-guix-substitute--key.pub"
                                                                   "(public-key
(ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
                                                 %default-authorized-guix-keys))))
                    ;; Remove GDM. We use wayland so prefer sddm
                    (delete gdm-service-type))))

(define %laptop-services
  (append
    (list
      (service tlp-service-type)
      (bluetooth-service #:auto-enable? #f)

      (service gnome-desktop-service-type))
    %pc-services))

(define %hacker-services
  (list
   (service docker-service-type)
   (service openssh-service-type)

   ; udev rules for 2factor authentication with yubikey
   (udev-rules-service 'libu2f-host libu2f-host
                       #:groups '("plugdev"))))

(define %pc-packages
  (append
    (list
      nss-certs
      fontconfig)
    %base-packages))


(define %hacker-packages
  (list
   sway))
